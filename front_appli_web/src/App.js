import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import PublicRouter from "@/pages/Public/PublicRouter";
import AdminRouter from "@/pages/Admin/AdminRouter";
import AuthPublicRouter from "@/pages/AuthPublic/AuthPublicRouter";
import PublicAuthGuard from "@/_helpers/PublicAuthGuard";
import NotAlreadyLogged from "@/_helpers/NotAlreadyLogged";
import AdminAuthGuard from "@/_helpers/AdminAuthGuard";
import NotAlreadyLoggedAdmin from "@/_helpers/NotAlreadyLoggedAdmin";
import AdminLogin from "@/pages/Admin/AdminLogin";
import "@/normalize.css";
import AuthLayout from "./pages/AuthLayout";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/auth" />} />
        <Route
          path="/auth/*"
          element={
            <NotAlreadyLogged>
              <AuthPublicRouter />
            </NotAlreadyLogged>
          }
        />
        <Route
          path="/*"
          element={
            <PublicAuthGuard>
              <PublicRouter />
            </PublicAuthGuard>
          }
        />
        <Route
          path="/admin/*"
          element={
            <AdminAuthGuard>
              <AdminRouter />
            </AdminAuthGuard>
          }
        />
        <Route
          path="/authadmin/*"
          element={
            <NotAlreadyLoggedAdmin>
              <AuthLayout>
                <AdminLogin />
              </AuthLayout>
            </NotAlreadyLoggedAdmin>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
