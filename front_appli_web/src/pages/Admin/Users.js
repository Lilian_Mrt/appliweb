import React, { useEffect, useState } from "react";
import { adminServices } from "../../_services/admin.services";
import trash from "@/imgs/supprimer.svg";

const Users = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await adminServices.getUsers();
      const data = response.data;
      setUsers(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const onClickDelete = async (e) => {
    console.log(e.target.id);
    await adminServices.deleteUser(e.target.id);
    // window.location.reload(true);
  };

  return (
    <>
      <div className="titleUsers">
        <span>Users :</span>
      </div>
      <div className="users">
        {users.map((user) => (
          <div className="user" id={"user_" + user.id} key={user.id}>
            <div className="userName">{user.name}</div>
            <div className="userFirstName">{user.firstName}</div>
            <img
              src={trash}
              alt="delete"
              id={user.id}
              onClick={onClickDelete}
            />
          </div>
        ))}
      </div>
    </>
  );
};

export default Users;
