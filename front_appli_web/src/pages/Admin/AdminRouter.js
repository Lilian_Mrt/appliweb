import React from "react";
import { Routes, Route } from "react-router-dom";
import { AdminLayout, Users } from "@/pages/Admin/";

const AdminRouter = () => {
  return (
    <Routes>
      <Route element={<AdminLayout />}>
        <Route index element={<Users />} />
        <Route path="/users" element={<Users />} />
      </Route>
    </Routes>
  );
};

export default AdminRouter;
