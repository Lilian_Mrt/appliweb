import React, { useState } from "react";
import { adminAccountServices } from "@/_services/adminAccount.services";
import { useNavigate } from "react-router-dom";

const AdminLogin = () => {
  const [credentials, setCredentials] = useState({
    login: "",
    password: "",
  });

  const [warning, setWarning] = useState("");

  const navigate = useNavigate();

  const onChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    const response = await adminAccountServices.adminLogin(
      credentials.login,
      credentials.password
    );
    console.log(response);
    if (response.data === "wrong") {
      setWarning("Acces denied");
    } else {
      adminAccountServices.saveTokenAdmin(response.data);
      navigate("/admin");
    }
  };

  return (
    <div className="authAdmin">
      <form onSubmit={onSubmit}>
        <label htmlFor="login">Login : </label>
        <input
          type="text"
          id="login"
          name="login"
          value={credentials.login}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="password">Password : </label>
        <input
          type="password"
          id="password"
          name="password"
          value={credentials.password}
          onChange={onChange}
          required
        />
        <br />
        <br />
        <div className="warnignAdmin">{warning}</div>

        <input type="submit" value="Connexion"></input>
      </form>
    </div>
  );
};

export default AdminLogin;
