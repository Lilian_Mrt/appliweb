import React from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import logo from "@/imgs/rockingchair.svg";
import "@/pages/Public/css/publicLayout.css";
import { adminAccountServices } from "../../_services/adminAccount.services";

const PublicLayout = () => {
  const navigate = useNavigate();

  const logout = () => {
    adminAccountServices.logoutAdmin();
    navigate("/authadmin");
  };

  return (
    <div className="AdminLayout">
      <header>
        <div className="leftHeader">
          <img src={logo} alt="Rockin Chair" className="logoMain" />
          <div>
            <span>Booking Chair</span>
          </div>
        </div>
        <div className="rightHeader">
          <span>Admin </span>
          <button onClick={logout}>Log out</button>
        </div>
      </header>
      <div className="basAdmin">
        <div className="sidebar">
          <ul>
            <li>
              <Link to="/admin/users">Users</Link>
            </li>
          </ul>
        </div>
        <div className="containerAdmin">
          <Outlet />
        </div>
      </div>
    </div>
  );
};

export default PublicLayout;
