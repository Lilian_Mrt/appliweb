import React from "react";
import { Routes, Route} from "react-router-dom";


import Error from "@/_utils/Error";

import {Main , Store, PublicLayout, TopUp} from "@/pages/Public/"

const PublicRouter = () => {
  return (
    <Routes>
      <Route element={<PublicLayout />}>
        <Route index element={<Main />} />
        <Route path="/main" element={<Main />} />
        <Route path="/store" element={<Store />} />
        <Route path="/topup" element={<TopUp />} />
        <Route path="*" element={<Error />} />
      </Route>
    </Routes>
  );
};

export default PublicRouter;
