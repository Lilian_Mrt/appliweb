import React, { useEffect, useState } from "react";
import { publicServices } from "@/_services/public.services";
import { useNavigate } from "react-router-dom";
import { accountServices } from "@/_services/account.services";

const Store = () => {
  const navigate = useNavigate();
  const [copiesOnSale, setCopiesOnSale] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response1 = await publicServices.getCopiesOnSale();
      const data1 = response1.data;
      setCopiesOnSale(data1);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const onClickBuy = async (e) => {
    const response = await publicServices.buyCopy(
      accountServices.getId(),
      e.target.id
    );
    alert(response.data);
    backToLibrary();
  };

  const backToLibrary = () => {
    navigate("/");
  };

  return (
    <div className="container">
      <span>COPIES ON SALE :</span>
      <div className="books">
        {copiesOnSale.map((copy) => (
          <div className="book" id={"book_" + copy.id} key={copy.id}>
            <div className="description">
              <div className="bookName">{copy.bookName}</div>
              <div className="bookAuthor">From {copy.bookAuthor}</div>
              <div className="language">{copy.language} Version </div>
              <div className="seller">Seller : {copy.nameOwner}</div>
              <div className="price"> At {copy.price}</div>
            </div>
            <button className="buyButton" id={copy.id} onClick={onClickBuy}>
              BUY
            </button>
          </div>
        ))}
      </div>
      <div className="foot">
        <button onClick={backToLibrary}>Go back to my Library</button>
      </div>
    </div>
  );
};

export default Store;
