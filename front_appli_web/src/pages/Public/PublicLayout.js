import React, { useEffect, useState } from "react";
import { publicServices } from "../../_services/public.services";
import { accountServices } from "../../_services/account.services";
import { Outlet, useNavigate } from "react-router-dom";
import logo from "@/imgs/rockingchair.svg";
import "@/pages/Public/css/publicLayout.css"

const PublicLayout = () => {
  const navigate = useNavigate();

  const [accountInfos, setAccountInfos] = useState([]);

  const logout = () => {
    accountServices.logout();
    navigate("/");
  };

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await publicServices.getProfileInfos(
        accountServices.getId()
      );
      const data = response.data;
      setAccountInfos(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const topUp	= () => {
    navigate("/topup")
  }

  return (
    <div className="PublicLayout">
      <header>
        <div className="leftHeader">
          <img src={logo} alt="Rockin Chair" className="logoMain" />
          <div><span>Booking Chair</span></div>
        </div>
        <div className="rightHeader">
          <span className="profil" id="firstName">
            {accountInfos.firstName}
          </span>
          <span className="profil" id="name">
            {accountInfos.name}
          </span>
          <span className="profil" id="money">
            Money : {accountInfos.money}
          </span>
          <button onClick={topUp}>Top up</button>
          <button onClick={logout}>Log out</button>
        </div>
      </header>
      <div className="container">
        <Outlet />
      </div>
    </div>
  );
};

export default PublicLayout;
