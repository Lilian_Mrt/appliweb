import React, { useState } from "react";
import { publicServices } from "../../_services/public.services";
import { accountServices } from "../../_services/account.services";
import { useNavigate } from "react-router-dom";

const TopUp = () => {
  const navigate = useNavigate();
  const [amount, setAmount] = useState("");
  const [warning, setWarning] = useState("");

  const confirmTopUp = async () => {
    const pattern = /^[0-9]+.?[0-9][0-9]?$/;
    if (pattern.test(amount)) {
      await publicServices.topUp(accountServices.getId(), amount);
      navigate("/");
    } else {
      setWarning("Price must be over 0 and can be float with 2 number max");
    }
  };

  const onChange = (e) => {
    setAmount(e.target.value);
  };

  const backToLibrary = (e) => {
    navigate("/main");
  };

  return (
    <div className="topup">
      <div className="amountTopUp">
        <input
          type="text"
          name="amount"
          value={amount}
          placeholder="Amount"
          onChange={onChange}
          required
        />
        <div id="priceWarning">{warning}</div>
        <button onClick={confirmTopUp}>Confirm</button>
      </div>
      <div className="footTopUp">
        <button onClick={backToLibrary}>Go back to my Library</button>
      </div>
    </div>
  );
};

export default TopUp;
