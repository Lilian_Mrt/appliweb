import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { accountServices } from "@/_services/account.services.js";
import { publicServices } from "@/_services/public.services";
import FormToAddBook from "@/components/FormToAddBook";
import "@/pages/Public/css/main.css";
import FormToSell from "@/components/FormToSell";
import trash from "@/imgs/supprimer.svg";
import FormToDelete from "@/components/FormToDelete";

const Main = () => {
  const navigate = useNavigate();
  const [ownersCopies, setOwnersCopies] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [showSell, setShowSell] = useState(false);
  const [toSell, setToSell] = useState(false);
  const [bookIdToTransmit, setBookIdToTransmit] = useState("");
  const [showDelete, setShowDelete] = useState(false);

  const handleStore = () => {
    navigate("/store");
  };

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await publicServices.getMyBooks(accountServices.getId());
      const data = response.data;
      setOwnersCopies(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const addBook = () => {
    setShowForm(true);
  };

  const onClickSell = async (e) => {
    setBookIdToTransmit(e.target.id);
    setToSell(e.target.innerText === "SALE");
    setShowSell(true);
  };

  const grayBgOnClick = () => {
    setShowSell(false);
    setShowForm(false);
    setShowDelete(false);
  };

  const onClickDelete = (e) => {
      setBookIdToTransmit(e.target.id);
      setShowDelete(true);
  }

  return (
    <>
      <div className="title">
        <span>My Library :</span>
      </div>
      <div className="books">
        {ownersCopies.map((copy) => (
          <div className="book" id={"book_" + copy.id} key={copy.id}>
            <div className="description">
              <div className="bookName">{copy.bookName}</div>
              <div className="bookAuthor">From {copy.bookAuthor}</div>
              <div className="language">{copy.language} Version </div>
            </div>
            <div className="bottomBook">
              <button onClick={onClickSell} id={copy.id}>
                {copy.selling ? <>ON SALE</> : <>SALE</>}
              </button>
              <img src={trash} alt="delete" id={copy.id} onClick={onClickDelete} />
            </div>
          </div>
        ))}
      </div>

      <div className="foot">
        <button onClick={handleStore}>Buy New Book</button>
        <button onClick={addBook}>Add a Book to my Library</button>
      </div>
      {(showForm || showSell || showDelete) && (
        <div className="grayBg" onClick={grayBgOnClick}></div>
      )}
      {showForm && <FormToAddBook />}
      {showSell && <FormToSell bookId={bookIdToTransmit} toSell={toSell} />}
      {showDelete && <FormToDelete copyId={bookIdToTransmit} />}
    </>
  );
};

export default Main;
