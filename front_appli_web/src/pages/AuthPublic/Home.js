import React from "react";
import { Link} from "react-router-dom";

const Home = () => {
  

  return (
    <div className="container">
      <ul>
        <li>
          <Link to="/auth/signin" replace className="Link">Sign In</Link>
        </li>
        <li>
          <Link to="/auth/signup" className="Link">Sign Up</Link>
        </li>
      </ul>
    </div>
  );
};
export default Home;
