import React from 'react';
import { Route, Routes } from 'react-router-dom';

import Error from '@/_utils/Error';

import { Home, SignIn, SignUp } from "@/pages/AuthPublic/";
import AuthLayout from '../AuthLayout';


const AuthPublicRouter = () => {
    return (
      <div>
        <Routes>
          <Route element={<AuthLayout />}>
          <Route index element={<Home />} />
          <Route path="/home" element={<Home />} />
          <Route path="/signin" element={<SignIn />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="*" element={<Error />} />
          </Route>
        </Routes>
      </div>
    );
};

export default AuthPublicRouter;