import React, { useState } from "react";

import { useNavigate } from "react-router-dom";
import { authServices } from "../../_services/auth.services";
import { accountServices } from "../../_services/account.services";

const SignUp = () => {
  const [credentials, setCredentials] = useState({
    name: "",
    firstName: "",
    mail: "",
    phoneNumber: "",
    password: "",
    confirm: "",
  });

  const [warning, setWarning] = useState([]);

  const navigate = useNavigate();

  const onChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (credentials.password === credentials.confirm) {
      const response = await authServices.createNewAccount(
        credentials.name,
        credentials.firstName,
        credentials.mail,
        credentials.phoneNumber,
        credentials.password
      );
      console.log(response.data)
      if (response.data === "Existing account") {
        setWarning(
          "This email already has an account on Booking Chair. Please Sign In"
        );
      } else {
        accountServices.saveTokenAndId(response.data);
        navigate("/main");
      }
    } else {
      setCredentials({
        ...credentials,
        password: "",
        confirm: "",
      });
      setWarning("Attention : Mot de passe différent");
    }
  };

  return (
    <div className="container">
      <h2>Sign Up</h2>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name : </label>
        <input
          type="text"
          id="name"
          name="name"
          value={credentials.name}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="firstName">First Name : </label>
        <input
          type="text"
          id="firstName"
          name="firstName"
          value={credentials.firstName}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="mail">Mail : </label>
        <input
          type="email"
          id="mail"
          name="mail"
          value={credentials.mail}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="phone">Phone Number : </label>
        <input
          type="tel"
          id="phoneNumber"
          name="phoneNumber"
          value={credentials.phoneNumber}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="password">Password :</label>
        <input
          type="password"
          id="password"
          name="password"
          value={credentials.password}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="confirm">Confirm :</label>
        <input
          type="password"
          id="confirm"
          name="confirm"
          value={credentials.confirm}
          onChange={onChange}
          required
        />
        <br />
        <span id="passwordWarning">{warning}</span>
        <br />

        <input type="submit" value="Sign Up"></input>
      </form>
    </div>
  );
};

export default SignUp;
