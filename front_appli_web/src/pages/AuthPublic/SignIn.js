import React, { useState } from "react";

import { useNavigate } from "react-router-dom";
import { accountServices } from "../../_services/account.services";

const SignIn = () => {
  const [credentials, setCredentials] = useState({
    mail: "",
    password: "",
  });

  const onChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };

  const navigate = useNavigate();

  const onSubmit = (e) => {
    e.preventDefault();
    accountServices.tryLoggingIn(credentials).then((res) => {
      if (res.data === "noAcc") {
        setCredentials({
          mail: "",
          password: ""
        })
        console.log("Redirect to Sign Up");
      } else if (res.data === "denied") {
        setCredentials({
          ...credentials,
          password: "",
        });
        console.log("Wrong password retry");
      } else {
        accountServices.saveTokenAndId(res.data);
        navigate("/main");
      }
    });
  };

  return (
    <div className="container">
      <h2>Connexion</h2>
      <form onSubmit={onSubmit}>
        <label htmlFor="email">Mail : </label>
        <input
          type="email"
          id="mail"
          name="mail"
          value={credentials.mail}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <label htmlFor="motdepasse">Password : </label>
        <input
          type="password"
          id="password"
          name="password"
          value={credentials.password}
          onChange={onChange}
          required
        />
        <br />
        <br />

        <input type="submit" value="Connexion"></input>
      </form>
    </div>
  );
};

export default SignIn;
