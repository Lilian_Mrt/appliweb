import React from 'react';
import logo from "@/imgs/rockingchair.svg";
import { Outlet } from 'react-router-dom';


const AuthLayout = ({children}) => {
    return (
      <div>
        <header>
          <img src={logo} alt="Rockin Chair" className="logoMain" />
          <div>
            <span>Booking Chair</span>
          </div>
        </header>
        <div className="containerAuth">
          {children}
          <Outlet/>
        </div>
      </div>
    );
};

export default AuthLayout;