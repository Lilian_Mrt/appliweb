import Axios from "@/_services/caller.services";

let getMyBooks = (id) => {
  return Axios.post("/hibernate/Servlet?op=getOwnerBooks&idOwner=" + id);
};

let getProfileInfos = (id) => {
  return Axios.post("/hibernate/Servlet?op=getProfileInfos&idAccount=" + id);
};

let getCopiesOnSale = () => {
  return Axios.post("/hibernate/Servlet?op=getCopiesOnSale");
};

let getBookForSelectOptions = () => {
  return Axios.post("/hibernate/Servlet?op=getBookForSelectOptions");
};

let addNewBookAndCopy = (id, bookName, bookAuthor, language) => {
  Axios.post(
    "/hibernate/Servlet?op=addNewBookAndCopy&idAccount=" +
      id +
      "&bookName=" +
      bookName +
      "&bookAuthor=" +
      bookAuthor +
      "&language=" +
      language
  );
};

let addNewCopy = (id, bookId, language) => {
  Axios.post(
    "/hibernate/Servlet?op=addNewCopy&idAccount=" +
      id +
      "&bookId=" +
      bookId +
      "&language=" +
      language
  );
};

let buyCopy = (idAccount, idCopy) => {
  return Axios.post(
    "/hibernate/Servlet?op=buyCopy&idAccount=" + idAccount + "&idCopy=" + idCopy
  );
};

let putToSell = (idCopy, price) => {
  Axios.post(
    "/hibernate/Servlet?op=putToSell&idCopy=" + idCopy + "&price=" + price
  );
};

let removeFromSell = (idCopy) => {
  Axios.post("/hibernate/Servlet?op=removeFromSell&idCopy=" + idCopy);
};

let topUp = (idAccount, amount) => {
  Axios.post(
    "/hibernate/Servlet?op=topUp&idAccount=" + idAccount + "&amount=" + amount
  );
};

let removeCopy = (idAccount, idCopy) => {
  Axios.post("/hibernate/Servlet?op=removeCopy&idAccount=" + idAccount + "&idCopy=" + idCopy);
}

export const publicServices = {
  getMyBooks,
  getProfileInfos,
  getCopiesOnSale,
  getBookForSelectOptions,
  addNewBookAndCopy,
  addNewCopy,
  buyCopy,
  putToSell,
  removeFromSell,
  topUp,
  removeCopy
};
