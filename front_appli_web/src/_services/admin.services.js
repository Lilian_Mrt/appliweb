import Axios from "@/_services/caller.services";

let getUsers = () => {
  return Axios.post(
    "/hibernate/Servlet?op=getUsers"
  );
};

let deleteUser = (id) => {
   Axios.post("/hibernate/Servlet?op=deleteUser&idAccount=" + id); 
}

export const adminServices = { getUsers, deleteUser };
