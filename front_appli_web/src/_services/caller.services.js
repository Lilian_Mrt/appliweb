import axios from "axios";

// Paramétrage de base d'axios
const Axios = axios.create({
  baseURL: "http://localhost:8080",
});

export default Axios;