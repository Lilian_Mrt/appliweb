import Axios from "@/_services/caller.services";

let adminLogin = (login, password) => {
    return Axios.post("/hibernate/Servlet?op=adminLogin&login=" + login + "&password=" + password);
}
let saveTokenAdmin = (data) => {
  sessionStorage.setItem("tokenAdmin", data);
};

let isLoggedAdmin = () => {
  let token = sessionStorage.getItem("tokenAdmin");
  return !!token;
};

let logoutAdmin = () => {
  sessionStorage.removeItem("tokenAdmin");
};

export const adminAccountServices = {
  saveTokenAdmin,
  isLoggedAdmin,
  logoutAdmin,
  adminLogin
};
