import Axios from "@/_services/caller.services";

let tryLoggingIn = (credentials) => {
  return Axios.post(
    "/hibernate/Servlet?op=publicAuth&mail=" +
      credentials.mail +
      "&password=" +
      credentials.password
  );
};

let saveTokenAndId = (data) => {
  const parts = data.split(";");
  sessionStorage.setItem("token", parts[0]);
  sessionStorage.setItem("id",parts[1]);
};

let isLogged = () => {
  let token = sessionStorage.getItem("token");
  return !!token;
};

let getId = () => {
   return sessionStorage.getItem("id");
};

let logout = () => {
  sessionStorage.removeItem("token");
  sessionStorage.removeItem("id");
};

export const accountServices = { tryLoggingIn, saveTokenAndId, getId, isLogged, logout };
