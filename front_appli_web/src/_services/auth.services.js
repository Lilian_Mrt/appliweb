import Axios from "@/_services/caller.services";


let createNewAccount = (nameAccount, firstName, mail, phoneNumber, password ) => {
    return Axios.post("/hibernate/Servlet?op=createAccount&name="+ nameAccount +"&firstName=" + firstName + "&mail=" + mail + "&phoneNumber=" + phoneNumber + "&password=" + password);
}

export const authServices = { createNewAccount };