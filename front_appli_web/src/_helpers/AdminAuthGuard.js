import React from 'react';
import { adminAccountServices } from '../_services/adminAccount.services';
import { Navigate } from 'react-router-dom';

const AdminAuthGuard = ({children}) => {
    if (!adminAccountServices.isLoggedAdmin()) {
      return <Navigate to="/authadmin" />;
    }

    return children;
};

export default AdminAuthGuard;