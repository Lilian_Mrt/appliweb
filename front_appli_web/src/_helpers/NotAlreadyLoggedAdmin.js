import React from 'react';
import { adminAccountServices } from '@/_services/adminAccount.services';
import { Navigate } from 'react-router-dom';

const NotAlreadyLoggedAdmin = ({children}) => {
    if (adminAccountServices.isLoggedAdmin()) {
      return <Navigate to="/admin" />;
    }
    return children;
};

export default NotAlreadyLoggedAdmin;