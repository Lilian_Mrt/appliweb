import { Navigate } from "react-router-dom";
import { accountServices } from "../_services/account.services";

const PublicAuthGuard = ({children}) => {

    if (!accountServices.isLogged()){
        return <Navigate to="/auth/home"/>
    }
    
    return children
};

export default PublicAuthGuard;