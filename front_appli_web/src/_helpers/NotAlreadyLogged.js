import React from 'react';
import { accountServices } from '../_services/account.services';
import { Navigate } from 'react-router-dom';

const NotAlreadyLogged = ({children}) => {
    if (accountServices.isLogged()) {
      return <Navigate to="/main" />;
    }
    return children;
};

export default NotAlreadyLogged;