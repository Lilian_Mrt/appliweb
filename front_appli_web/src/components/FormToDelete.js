import React from 'react';
import { publicServices } from '../_services/public.services';
import { accountServices } from '../_services/account.services';

const FormToDelete = (data) => {
    const onClickConfirmRemove = async () => {
        await publicServices.removeCopy(accountServices.getId(),data.copyId);
        window.location.reload(true);
    }

    return (
      <div className='removeCopy'>
          Remove this book of your library?
          <button className="confirmRemove" onClick={onClickConfirmRemove}>
            Confirm
          </button>
        </div>
    );
};

export default FormToDelete;