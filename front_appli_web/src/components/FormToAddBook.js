import React, { useEffect, useState } from "react";
import Select from "react-select";
import { publicServices } from "../_services/public.services";
import { accountServices } from "../_services/account.services";
import { LanguageList } from "../_utils/LanguageList";

const FormToAddBook = () => {

  const [credentials, setCredentials] = useState({
    bookName: "",
    bookAuthor: "",
    bookId: -1,
    language: "",
    selectorValue:""
  });

  const [options, setOptions] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await publicServices.getBookForSelectOptions();
      const data = response.data;
      setOptions(data);
      setOptions((prevState) => [
        ...prevState,
        { label: "Not Already Existing Book", value: -1 },
      ]);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const selectorOnChange = (e) => {
    if (e.value !== -1) {
      const words = e.label.split(" of ");
      setCredentials({
        ...credentials,
        bookId: e.value,
        bookName: words[0],
        bookAuthor: words[1],
      });
    } else {
      setCredentials({
        ...credentials,
        bookId: e.value,
        bookName: "",
        bookAuthor: "",
      });
    }
  };

  const onChangeLanguage = (e) => {
    setCredentials({
      ...credentials,
      language: e.label,
    });
  };

  const onChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
      bookId : -1
    });
  };

  const onSubmit = async (e) => {
    if (credentials.bookId === -1) {
      await publicServices.addNewBookAndCopy(
        accountServices.getId(),
        credentials.bookName,
        credentials.bookAuthor,
        credentials.language,
      );
    } else {
      await publicServices.addNewCopy(
        accountServices.getId(),
        credentials.bookId,
        credentials.language,
      );
    }
    window.location.reload(true);
  };

  return (
    <>
    <div className="addBookDiv">
      <div className="addBookTitle">Add a new Book</div>
      <Select
        options={options}
        onChange={(e) => {
          selectorOnChange(e);
        }}
      />
      
      <input
        type="text"
        name="bookName"
        id="bookName"
        placeholder="Book"
        value={credentials.bookName}
        onChange={onChange}
        required
      />
      <input
        type="text"
        name="bookAuthor"
        id="bookAuthor"
        placeholder="Author"
        value={credentials.bookAuthor}
        onChange={onChange}
        required
      />
      <Select
        options={LanguageList.languages_list}
        onChange={(e) => {
          onChangeLanguage(e);
        }}
        required
      />
      <button onClick={onSubmit}>Confirm</button>
    </div>
    </>
  );
};

export default FormToAddBook;
