import React from "react";
import Select from "react-select";

const options = [
  { value: "chocolate", label: "Chocolate of lo" },
  { value: "strawberry", label: "Strawberry of gtz" },
  { value: "vanilla", label: "Vanilla of eazraz" },
];
const onChange = (e) => {
    const words = e.label.split(' ');
    console.log(words[0]);
    console.log(words[2]);
}

const MyComponent = (value) => <Select onChange={onChange} options={options} value={value} />;

export const selectorBook = { MyComponent };
