import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { publicServices } from "@/_services/public.services";

const FormToSell = (data) => {
  const navigate = useNavigate();
  const [price, setPrice] = useState("");
  const [warning, setWarning] = useState("");

  const onChange = (e) => {
    setPrice(e.target.value);
  };

  const onClickConfirmSell = async () => {
    const pattern = /^[0-9]+.?[0-9][0-9]?$/;
    if (pattern.test(price)) {
      await publicServices.putToSell(data.bookId, price);
      window.location.reload(true);
    } else {
      setWarning("Price must be over 0 and can be float with 2 number max");
    }
  };

  const onClickConfirmRemove = async () => {
    await publicServices.removeFromSell(data.bookId);
    window.location.reload(true);
  };

  if (data.toSell) {
    return (
      <div className="formToSell">
        <div className="priceDiv">
          <input
            type="text"
            name="price"
            value={price}
            placeholder="Price"
            onChange={onChange}
            required
          />
          <br></br>
          <div id="priceWarning">{warning}</div>
        </div>

        <button className="confirmSell" onClick={onClickConfirmSell}>
          Confirm
        </button>
      </div>
    );
  } else {
    return (
      <div className="removeFromSell">
        Remove this book from the store?
        <button className="confirmRemove" onClick={onClickConfirmRemove}>
          Confirm
        </button>
      </div>
    );
  }
};

export default FormToSell;
