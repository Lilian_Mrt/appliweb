const languages_list = [
    {label: "Arabic", code: "ar"},
    {label: "Breton", code: "br"},
    {label: "Catalan", code: "ca"},
    {label: "Chinese", code: "zh"},
    {label: "Croatian", code: "hr"},
    {label: "Czech", code: "cs"},
    {label: "Danish", code: "da"},
    {label: "Dutch", code: "nl"},
    {label: "English", code: "en"},
    {label: "French", code: "fr"},
    {label: "German", code: "de"},
    {label: "Italian", code: "it"},
    {label: "Korean", code: "ko"},
    {label: "Portuguese", code: "pt"},
    {label: "Russian", code: "ru"},
    {label: "Spanish", code: "es"},
];

export const LanguageList = {
  languages_list
};